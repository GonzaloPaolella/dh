<?php

session_start();
if ($_POST) {
  if (isset($_POST['nombre']) ? $_SESSION['nombre'] = $_POST['nombre'] : "");
  if (isset($_POST['colorPreferido']) ? $_SESSION['color'] = $_POST['colorPreferido'] : "");
}

$nombre = isset($_SESSION['nombre']) ? $_SESSION['nombre'] : 'Usuario';
$color = isset($_SESSION['color']) ? $_SESSION['color'] : 'grey';

if (isset($_POST['resetear']) ? $color = 'grey' : "");

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
  <style>

    .link {
      display:inline-block;
      padding:8px;
      text-decoration:none;
      background-color: <?= $color ?>;
      color:white;
      margin-top: 8px;
    }

  </style>
</head>
<body class="link">

  <h1>Te damos la bienvenida <?= $nombre ?> </h1>

  <form action="" method="post">
    <input type="submit" name="resetear" value="Resetear colo de fondo">
  </form>

<a href="home.php" class="link">VOLVER...</a>

</body>
</html>
