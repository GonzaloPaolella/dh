<!DOCTYPE html>
<?php
  require_once('validationController.php');
 ?>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">

    <!-- My CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Registro</title>
  </head>

  <body class="align-content-center m-auto">
    <div class="container py-5">
      <div class="row justify-content-spacearound align-items-center border py-4 col-9 mx-auto">
        <div class="col-10 mx-auto">
          <form class="" action="" method="post">
            <div class="form-group mb-2">
              <label class="w-100">
                <p>Nombre:</p>
                <input class="form-control" type="text" name="name" placeholder="Nombre..." value="<?= (isset($_POST['name']))? $_POST['name'] : "" ?>">
                <?php if (isset($errors['name'])): ?>
                  <p class="mb-1 mt-2 text-danger"><?= $errors['name'] ?></p>
                <?php endif; ?>
              </label>
            </div>
            <div class="form-group">
              <label class="w-100">
                <p>Email:</p>
                <input class="form-control" type="text" name="email" placeholder="Email..." value="<?= isset($email) ? $email : "" ?>">
                <?php if (isset($errors['email'])): ?>
                  <p class="mb-1 mt-2 text-danger"><?= $errors['email'] ?></p>
                <?php endif; ?>
              </label>
            </div>
            <div class="form-group">
              <label class="w-100">
                <p>Contraseña:</p>
                <input class="form-control" type="password" name="pass" placeholder="Contraseña...">
                <?php if (isset($errors['pass'])): ?>
                  <p class="mb-1 mt-2 text-danger"><?= $errors['pass'] ?></p>
                <?php endif; ?>
              </label>
            </div>
            <div class="form-group">
              <label class="w-100">
                <p>Confirmar contraseña:</p>
                <input class="form-control" type="password" name="passConfirm" placeholder="Confirmar contraseña...">
                <?php if (isset($errors['passConfirm'])): ?>
                  <p class="mb-1 mt-2 text-danger"><?= $errors['passConfirm'] ?></p>
                <?php endif; ?>
              </label>
            </div>

            <button class="btn btn-primary w-25" type="submit" name="">Registrarme</button>

          </form>

        </div>

      </div>


    </div>
  </body>
</html>
