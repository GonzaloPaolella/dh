<?php

  $errors = [];

  /* Verificamos si hay POST */
  if ($_POST) {

    /* Verificamos que el campo de Nombre sea valido */
    if (strlen($_POST['name']) > 0) {
      if (strlen($_POST['name']) < 3) {
        $_POST['name'] = null;
        $errors['name'] = "El campo tiene que tener al menos 3 caracteres.";
      }
    }
    else{
      $errors['name'] = 'El campo esta vacío.';
    }

    /* Verificamos que el campo de Email sea valido*/
    if (strlen($_POST['email']) > 0) {
      if (!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
        $_POST['email'] = null;
        $errors['email'] = "El email ingresado no es valido.";
      }
    }
    else{
      $errors['email'] = "El campo esta vacío.";
    }

    /* Verificamos que que el campo de Contraseña sea valido */
    if (strlen($_POST['pass']) > 0) {
      if (strlen($_POST['pass']) < 6) {
        $errors['pass'] = "La contraseña debe tener al menos 6 caracteres.";
      }
    }
    else{
      $errors['pass'] = "El campo esta vacío.";
    }

    /* Verficamos que el campo de Confirmar Contraseña sea valido y que coincida*/
    if (strlen($_POST['passConfirm']) > 0) {
      if ($_POST['passConfirm'] != $_POST['pass']) {
        $errors['passConfirm'] = "La contraseñas no coinciden.";
      }
    }
    else{
      $errors['passConfirm'] = "El campo esta vacío.";
    }

    /* Si NO hay errores creamos variables locales con los datos y hasheamos la clave */
    if (is_null($errors)) {
      $name = $_POST['name'];
      $email = $_POST['email'];
      $pass = password_hash($_POST['pass'] , PASSWORD_DEFAULT);

      /* Creamos el array USER*/
      $user = [
        'name' => $name,
        'email' => $email,
        'pass' => $pass
      ];
    }
    /* Si esta seteado el USER traemos el archivo JSON y verificamos
       si el mail esta en uso */
    if (isset($user)){
      $fileUsers = file_get_contents('users.json');
      $users = json_decode($fileUsers,true);

      /* Si el array de usuarios esta vacio agregamos al users*/
      if ($users == null) {
        $users[] = $user;
        $users['id'] = 1;
      }else{
        for ($i=0; $i < count($users) ; $i++) {
           if ($i['email'] == $user['email']) {
             $errors['email'] = "El email ingresado ya esta en uso.";
             exit;
           }
        }
      }
      /* Le sumamos 1 al ultimo ID guardado y lo asignamos al USER*/
      $user['id'] = end($users)['id'] + 1;
      $users[] = $user;
      $jsonUsers = json_encode($users);
      file_put_contents('users.json',$jsonUsers);
    }
}

?>
