<!DOCTYPE html>
<?php
  $nombre = "Gonzalo Paolella";
  $numero = rand(0,1);
  $links = [
    'Google' => 'https://google.com',
    'Facebook' => 'https://facebook.com',
    'Twitter' => 'https://twitter.com'
  ];
 ?>

<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio de Embed</title>
  </head>
  <body>
    <h1>La Internet</h1>
    <marquee>Bienvenidos al mundo de la internet</marquee>
    <h2>Bienvenido <?=$nombre?></h2>
    <h3>¿Es usted un ganador?</h3>
    <?php if ($numero == 1) : ?>
      <h4>SI</h4>
    <?php endif ?>
    <?php if ($numero == 0) : ?>
      <h4>NO</h4>
    <?php endif ?>

    <h3>Algunos sitios interesantes:</h3>
    <ul>
      <?php foreach ($links as $brand => $link) : ?>
        <li>
          <a href=<?="$link"?>><?=$brand?></a>
        </li>
      <?php endforeach ?>
    </ul>
  </body>
