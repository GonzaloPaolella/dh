<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Ejercicios de Ciclos</title>
  </head>
  <body>
    <header>
      <h1>Ejercicios de Ciclos</h1>
    </header>
    <main>
      <h3>Ejercicio 1</h3>
      <?php
        for ($i=1; $i < 10 ; $i++) {
          echo "$i x 2 = " . $i*2 . "<br>";
        }
       ?>

       <h3>Ejercicio 2</h3>
       <?php
        $i = 100;
        while ($i >= 85) {
          echo $i . "<br>";
          $i-=1;
        }
       ?>

       <h3>Ejercicio 3</h3>
       <?php
        $contador = 1;
        while ($contador <= 5) {
          echo $contador*2 . "<br>";
          $contador++;
        }
        ?>

        <h3>Ejercicio 4</h3>
        <?php
          $contador = 0;
          $tiros = 0;
          while ($contador <= 5) {
            $random = rand(0,1);
            $tiros += 1;
            if ($contador == 5){
              echo $tiros . "<br>";
              echo $contador;
              break;
            }
            if ($random == 1){
              $contador += 1;
            }
          }
         ?>

         <h3>Ejercicio 5</h3>
         <?php
          $array = ['Gonzalo', 'Juan', 'Valentin', 'Lucas', 'Matias'];
          echo "Con FOR: <br>";
          for ($i=0; $i < 5 ; $i++) {
            echo $array[$i] . "<br>";
          }

          echo "<br>";
          $i = 0;
          echo "Con WHILE: <br>";
          while ($i < 5) {
            echo $array[$i] . "<br>";
            $i += 1;
          }

          echo "<br>";
          $i = 0;
          echo "Con DO/WHILE: <br>";
          do {
            echo $array[$i] . "<br>";
            $i += 1;
          } while ($i < 5);

          echo "<br>";
          echo "Con FOREACH: <br>";
          foreach ($array as $nombre) {
            echo $nombre . "<br>";
          }
          ?>

           <h3>Ejercicio 6</h3>
           <?php
           $numeros = [];
            for ($i=0; $i < 10 ; $i++) {
              $numeros[] = rand(0,10);
            }
            foreach ($numeros as $numero) {
              if ($numero != 5) {
                echo "$numero <br>";
              }else {
                echo "Se ecnontro un 5!";
                break;
              }
            }
           ?>

           <h3>Ejercicio 7</h3>
           <?php
            $contador = 0;
            $numeros = [];
            for ($i=0; $i < 10 ; $i++) {
              $numeros[] = rand(0,100);
            }
            foreach ($numeros as $numero) {
              if ($numero % 2 == 0) {
                $contador += 1;
              }
            }
            echo $contador;
            ?>

            <h3>Ejercicio 8</h3>
            <?php
              $mascota = [
                'animal' => 'gato',
                'edad' => 5,
                'altura' => 0.60,
                'nombre' => 'Juana'
              ];
              foreach ($mascota as $key => $value) {
                echo "$key : $value <br>";
              }
            ?>

            <h3>Ejercicio 9</h3>
            <?php
              $ceu = ["Italia"=>"Roma", "Luxembourg"=>"Luxembourg", "Bélgica"=>"Bruselas", "Dinamarca"=>"Copenhagen", "Finlandia"=>"Helsinki", "Francia" =>
                      "Paris", "Slovakia"=>"Bratislava", "Eslovenia"=>"Ljubljana", "Alemania" => "Berlin",
                      "Grecia" => "Athenas", "Irlanda"=>"Dublin", "Holanda"=>"Amsterdam",
                      "Portugal"=>"Lisbon", "España"=>"Madrid", "Suecia"=>"Stockholm", "Reino
                      Unido"=>"London", "Chipre"=>"Nicosia", "Lithuania"=>"Vilnius", "Republica
                      Checa"=>"Prague", "Estonia"=>"Tallin", "Hungría"=>"Budapest", "Latvia"=>"Riga",
                      "Malta"=>"Valletta", "Austria" => "Vienna", "Polonia"=>"Warsaw"] ;
              foreach ($ceu as $key => $value) {
                echo "$value, $key <br>";
              }
             ?>

             <h3>Ejericio 10</h3>
             <?php
              $ceu = [
                    "Argentina" => [
                                    'ciudades' => ["Buenos Aires", "Córdoba", "Santa Fé"],
                                    'esAmericano' => true
                                  ],
                      "Brasil" => [
                                  'ciudades' => ["Brasilia", "Rio de Janeiro", "Sao Pablo"],
                                  'esAmericano' => true
                                ],
                      "Colombia" => [
                                    'ciudades' => ["Cartagena", "Bogota", "Barranquilla"],
                                    'esAmericano' => true
                                  ],
                      "Francia" => [
                                    'ciudades' => ["Paris", "Nantes", "Lyon"],
                                    'esAmericano' => false
                                  ],
                      "Italia" => [
                                  'ciudades' => ["Roma", "Milan", "Venecia"],
                                  'esAmericano' => false
                                ],
                      "Alemania" => [
                                    'ciudades' => ["Munich", "Berlin", "Frankfurt"],
                                    'esAmericano' => false
                                  ]
                                ];

              ?>

              <?php

                foreach ($ceu as $pais => $datosPais) :
                  if ($datosPais['esAmericano']) : ?>
                    <span>Las ciudades de <?= $pais ?> son:</span>
                    <ul>
                      <?php foreach ($datosPais['ciudades'] as $ciudad): ?>
                        <li><?= $ciudad ?></li>
                      <?php endforeach; ?>
                    </ul>
                    <p>El país es americano.</p>
                  <?php endif ?>
                <?php endforeach ?>


    </main>
  </body>
</html>
