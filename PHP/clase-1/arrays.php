<?php
  $persona = [
    "nombre" => "Jon",
    "apellido" => "Snow",
    "edad" => 27,
    "hobbies" => ["Netflix", "Futbol", "Programar"],
  ];
  print_r($persona);

  $persona["edad"] = 25;

  print_r($persona);

  $persona["direccion"] = "Winterfell";

  print_r($persona);

  $persona["hobbies"][] = "Musica";

  print_r($persona);
 ?>
