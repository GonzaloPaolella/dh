<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    <?php
    // PUNTO 1
    $numero1 = 15;
    $numero2 = 10;

    if ($numero1 > $numero2) {
      echo "El numero mayor es " . $numero1;
    }
    else {
      if ($numero1 == $numero2) {
        echo "Los numeros son iguales";
      }
      else {
        echo "El numero mayor es " . $numero2;
      }
    }
    ?>

    <br>
    <br>

    <?php
    // PUNTO 2
    $numero = rand(1,5);
    if ($numero == 3 || $numero == 5) {
      echo "Ganaste! El numero es " . $numero;
    }
    else {
      echo "Perdiste! El numero no es 3 ni 5";
    }
    ?>

    <br>
    <br>

    <?php
    // PUNTO 3 Y 4
      $numero = rand(1,100);
      if ($numero > 50) {
        echo "El numero " . $numero . " es mayor que 50";
      }
      elseif ($numero < 50) {
        echo "El numero " . $numero . " es menor que 50";
      }
      else {
        echo "El numero es igual a 50";
      }
     ?>

     <br>
     <br>

     <?php
     // PUNTO 5
      $nombreDeUsuario = "admi";
      $contraseniaDeUsuario = "134";

      if ($nombreDeUsuario == "admin") {
        if ($contraseniaDeUsuario == "1234") {
          echo "Bienvenido!";
        }
        else {
          echo "La contraseña es incorrecta";
        }
      }
      elseif ($contraseniaDeUsuario == "1234") {
        if ($nombreDeUsuario != "admin") {
          echo "El usuario es incorrecto";
        }
      }
      else {
        echo "Los datos ingresados son incorrectos";
      }
     ?>

     <br>
     <br>

     <?php
     // PUNTO 6
      $edad = 20;
      $casado = true;
      $sexo = "Masculino";

      if ($edad >= 18 && $sexo == "Masculino") {
        echo "Bienvenido!";
      }
      elseif ($sexo == "Otro") {
        echo "Bienvenido!";
      }
     ?>

     <br>
     <br>

     <?php
     // PUNTO 6
      $cantidadDeAlumnos = 1;

      if ($cantidadDeAlumnos) {
        echo "true";
      }
      else {
        echo "false";
      }
      ?>

      <br>
      <br>

      <?php
      // PUNTO 8
        $numero = rand(1,10);
        $parInpar = ($numero % 2 == 0) ? true : false  ;

        if ($parInpar) {
          echo "El numero es par";
        }
        else {
          echo "El numero es inpar";
        }
      ?>

      <br>
      <br>

      <?php
      // PUNTO 9
        $nota = rand(-10,10);

        switch ($nota) {
          case 1:
          case 2:
          case 3:
            echo "Desaprobado";
            break;
          case 4:
          case 5:
            echo "Zafamos";
            break;
          case 6:
          case 7:
          case 8:
            echo "Bien!!";
            break;
          case 9:
            echo "MUY bien!!";
            break;
          case 10:
            echo "Excelente!!!!";
            break;
          default:
            echo "El numero no esta entre 1 y 10!";
            break;
        }
       ?>
  </body>
</html>
