<?php
// Definir una función mayor() que reciba 3 números y devuelva el mayor.
// Definir una función tabla() que reciba un parámetro base, un parámetro límite, y devuelve un array con la secuencia de números desde el numero base hasta el numero limite.
// Modificar mayor() para que si recibe sólo 2 parámetros, compare a esos dos números con el número 100.
// Modificar tabla para que si recibe un sólo parámetro utilice el número 100.

  function mayor($numero1, $numero2, $numero3){
    global $funcionesEjecutadas;
    $funcionesEjecutadas++;
    $mayor = 0;
    if ($numero1 > $numero2 && $numero1 > $numero3){
      $mayor = $numero1;
    }
    elseif ($numero2 > $numero1 && $numero2 > $numero3) {
      $mayor = $numero2;
    }
    elseif ($numero3 > $numero1 && $numero3 > $numero2) {
      $mayor = $numero3;
    }
    return $mayor;
  }

  function tabla($base,$limite){
    global $funcionesEjecutadas;
    $funcionesEjecutadas++;
    $array = [];
    for ($i=$base; $i <= $limite ; $i++) {
      $array [] = $i;
    }
    return $array;
  }

  function mayorEditado($numero1, $numero2, $numero3 = 100){
    global $funcionesEjecutadas;
    $funcionesEjecutadas++;
    if ($numero1 > $numero2 && $numero1 > $numero3){
      return $numero1;
    }
    elseif ($numero2 > $numero1 && $numero2 > $numero3) {
      return $numero2;
    }
    return $numero3;
  }

  function tablaEditado($base,$limite = 100){
    global $funcionesEjecutadas;
    $funcionesEjecutadas++;
    $array = [];
    for ($i=$base; $i <= $limite ; $i++) {
      $array [] = $i;
    }
    return $array;
  }
 ?>
