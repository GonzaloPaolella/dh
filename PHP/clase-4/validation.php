<?php

  require_once('functions.php');

  $user=[
    'name' => $_POST['name'],
    'email' => $_POST['email'],
    'pass' => $_POST['pass']
  ];

  $errors = [];
  /* VALIDACION GENERAL */
  if ($_POST) {

    /* Validacion de NOMBRE */
    if (strlen($user['name']) == 0) {
      $errors ['name'] = "El campo del nombre esta vacío.";
    } elseif (strlen($user['name']) < 3){
      $errors ['name'] = "El nombre debe tener al menos 3 caracteres.";
    }

    /* Validacion de EMAIL */
    if (strlen($user['email']) == 0) {
      $errors['email'] = "El campo del email esta vacío.";
    }elseif (!filter_var($user['email'], FILTER_VALIDATE_EMAIL)) {
      $errors['email'] = "El email tiene que tener un formato valido.";
    }

    /* Validacion de PASSWORD*/
    if (strlen($user['pass']) == 0) {
      $errors['pass'] = "El campo de la contraseña esta vacío.";
    }elseif (strlen($user['pass']) < 8) {
      $errors['pass'] = "La contraseña debe tener al menos 8 caracteres.";
    }



    /* Si no hay errores redirigimos al usuarios a IMPRIMIR.PHP*/
    if (!$errors){
      guardarUsuario($user);
    }
  }
 ?>
