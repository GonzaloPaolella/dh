<?php

    class User{
        private $nombre;
        private $email;
        /*
         * @var $celular Celular
         */
        private $celular;
        private $password;
        
        function __construct($nombre, $email, Celular $celular, $password){
            $this->nombre = $nombre;
            $this->email = $email;
            $this->celular = $celular;
            $this->setPassword($password);
        }

        function saludar(){
            return 'Hola ' . $this->nombre;
        }

        private function passwordHash($password){
            return password_hash($password, PASSWORD_DEFAULT);
        }

        function mostrarTelefono(){
            return 'Tengo un telefono ' . $celular->marca . ', el modelo es ' . $celular->modelo . ', la linea es ' . $celular->proveedor . ' y el numero es: ' . $celular->numero;
        }

        function getNombre(){
            return $this->nombre;
        }

        function getEmail(){
            return $this->email;
        }

        function getCelular(){
            return $this->celular;
        }

        function getPassword(){
            return $this->password;
        }

        function setNombre($nombre){
            $this->nombre = $nombre;
        }

        function setEmail($email){
            $this->email = $email;
        }

        function setPassword($password){
            $this->password = $this->passwordHash($password);
        }

    }
