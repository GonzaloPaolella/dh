<?php

    class Celular{
        private $marca;
        private $modelo;
        private $proveedor;
        private $numero;

        function __contruct($marca, $modelo, $proveedor, $numero){
            $this->marca = $marca;
            $this->modelo = $modelo;
            $this->proveedor = $proveedor;
            $this->numero = $numero;
        }

        function getMarca(){
            return $this->marca;
        }

        function getModelo(){
            return $this->modelo;
        }

        function getProveedor(){
            return $this->proveedor;
        }

        function getNumero(){
            return $this->numero;
        }

        function setMarca($marca){
            $this->marca = $marca;
        }

        function setModelo($modelo){
            $this->modelo = $modelo;
        }
        
        function setProveedor($proveedor){
            $this->proveedor = $proveedor;
        }

        function setNumero($numero){
            $this->numero = $numero;
        }

    }
?>