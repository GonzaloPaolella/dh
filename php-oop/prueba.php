<?php

require_once('usuario.php');
require_once('celular.php');

$celularGonzalo = new Celular('Apple', 'Iphone', 'Movistar', '1130114206');

$celularJuan = new Celular('Xiaomi', 'Note 7', 'Claro', '1155059878');

$gonzalo = new User('Gonzalo', 'gonzalo@nest.com.ar', $celularGonzalo, '123123');

$juan = new User('Juan', 'juan@nest.com.ar', $celularJuan, '456456');

var_dump($gonzalo);
var_dump($juan);

var_dump($gonzalo->mostrarTelefono());
