<?php
    $dsn = 'mysql:host=127.0.0.1;dbname=movies_db;port=3306;charset=utf8';
    $username = 'root';
    $pass = '';

    $db = new PDO($dsn, $username, $pass);

    function getAllSeries(PDO $db){
        $query = $db -> prepare("SELECT title FROM series");
        $query -> execute();

        $series = $db -> fetchAll();

        return $series;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Series</title>
</head>
<body>

    <div class="series-container">
        <?php foreach (getAllSeries() as $serie) : ?>
            <li><a href="serie.php">$serie['title']</a></li>
        <?php endforeach ?>
    </div>
    
</body>
</html>